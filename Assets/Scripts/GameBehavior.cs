﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using CustomExtensions;

public class GameBehavior : MonoBehaviour, IManager
{
    public class GenericCollection<T>
    {
        
    }

    GenericCollection<int> highScores = new GenericCollection<int>();

    public string labelText = "Collectionnez les 4 objets et gagnez votre liberté.";
    public int maxItems = 4;
    public bool showWinScreen = false;
    public bool showLoseScreen = false;
    public Stack<string> lootStack = new Stack<string>();
    public delegate void DebugDelegate(string newText);
    public DebugDelegate debug = Print;

    private int _itemsCollected = 0;
    private int _playerHP = 3;
    private string _state;
 
    public int Items
    {
        get { return _itemsCollected; }
        set
        {
            _itemsCollected = value;
            if (_itemsCollected == maxItems)
            {
                labelText = "Vous avez trouvé tous les objets !";
                showWinScreen = true;
                Time.timeScale = 0f;
            }
            else
            {
                labelText = "Objet trouvé, il en reste encore " + (maxItems - _itemsCollected) + ".";
            }
        }
    }

    public int HP
    {
        get { return _playerHP; }
        set
        {
            _playerHP = value;
            if (_playerHP <= 0)
            {
                showLoseScreen = true;
                Time.timeScale = 0f;
            }
            else
            {
                labelText = "J'ai été touché !";
            }
        }
    }

    public string State
    {
        get { return _state; }
        set { _state = value; }
    }

    public void Initialize()
    {
        _state = "Manager installé !";
        _state.DroleDeDebug();
        Debug.Log(_state);
        LogWithDelegate(debug);
        lootStack.Push("Epée de l'enfer");
        lootStack.Push("HP+");
        lootStack.Push("Clé en or");
        lootStack.Push("Bottes volantes");
        lootStack.Push("Le bracelet magique");
    }

    public void HandlePlayerJump()
    {
        debug("Le joueur a sauté");
    }

    public static void LogWithDelegate(DebugDelegate del)
    {
        del("Delegation de la tache de debug");
    }

    public static void Print(string newText)
    {
        Debug.Log(newText);
    }

    public void PrintLootReport()
    {
        var currentItem = lootStack.Pop();
        var nextItem = lootStack.Peek();
        Debug.LogFormat("Il y a {0} trésors qui vous attendent", lootStack.Count);
        Debug.LogFormat("Vous avez {0}, il vous reste à trouver {1}", currentItem, nextItem);
    }

    private void OnGUI()
    {
        GUI.Box(new Rect(20, 20, 150, 25), "Santé du joueur : " + _playerHP);
        GUI.Box(new Rect(20, 50, 150, 25), "Objets collectés : " + _itemsCollected);
        GUI.Label(new Rect(Screen.width / 2 - 100, Screen.height - 50, 300, 50), labelText);

        if (showWinScreen)
        {
            if (GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 50, 200, 100), "Vous avez gagné !"))
            {
                Utilities.RestartLevel(0);
            }
        }

        if (showLoseScreen)
        {
            if (GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 50, 200, 100), "Vous avez perdu !"))
            {
                try
                {
                    Utilities.RestartLevel(-1);
                    debug("Le jeu redémarre sans problème");
                }
                catch (System.ArgumentException e)
                {
                    Utilities.RestartLevel(0);
                    debug("Retour vers la scene d'id 0" + e.ToString());
                }
                finally
                {
                    debug("Restart");
                }
            }
        }
    }

    void RestartLevel()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1.0f;
    }

    // Start is called before the first frame update
    void Start()
    {
        Initialize();
        InventoryList<string> inventoryList = new InventoryList<string>();
        inventoryList.SetItem("Potion");
        Debug.Log(inventoryList.item);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
