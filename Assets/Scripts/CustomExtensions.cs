﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomExtensions
{
    public static class StringExtensions
    {
        public static void DroleDeDebug(this string str)
        {
            Debug.LogFormat("Cette chaine contient {0} caractères.", str.Length);
        }
    }
}