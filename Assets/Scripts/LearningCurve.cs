﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LearningCurve : MonoBehaviour
{
  
    private int currentAge = 30;
    public int addedAge = 1;
    public int currentGold = 32;
    public int playerLives = 3;
    public int diceRoll = 7;

 
    public float pi = 3.14f;


    public string firstName = "David";
    public string rareItem = "Relique";
    public string characterAction = "Attaque";

    public bool isAuthor = true;
    public bool pureOfHeart = true;
    public bool hasSecretIncantation = false;

    private Transform camTransform;
    public GameObject directionLight;
    private Transform lightTransform;

    Vector3 origin = new Vector3(0f, 0f, 0f);
    Vector3 forwardDirection = Vector3.forward;

    // Start is called before the first frame update
    void Start()
    {
        ComputeAge();

        Debug.Log($"Une chaine peut avoir des variables comme {firstName} inséreré directement!");

        
        int characterLevel = 32;
        int nextSkillLevel = GenerateCharacter("Florian", characterLevel);
        Debug.LogFormat("Prochaines compétences au niveau {0}", nextSkillLevel);


        PocketChange();
        OpenTreasureChamber();
        SwitchingAround();

    
        List<string> questPartyMembers = new List<string>() 
        { "Connan le Barbare", "Merlin le Magicien", "Deadpool est unique" };

        questPartyMembers.Add("Terminator est de retour");
        questPartyMembers.Insert(1, "Tanos le King");
        questPartyMembers.Remove("Connan le Barbare");

        Debug.LogFormat("Membres de la quête: {0}", questPartyMembers.Count);

        Dictionary<string, int> itemInventory = new Dictionary<string, int>()
        {
            { "Potion", 5},
            { "Antidote", 7},
            { "Aspirine", 1}
        };

        itemInventory["Potion"] = 10;
        itemInventory.Add("Shuriken", 3);
        itemInventory["Pansement"] = 5;

        if(itemInventory.ContainsKey("Aspirine"))
        {
            itemInventory["Aspirine"] = 3;
        }

        itemInventory.Remove("Antidote");
        Debug.LogFormat("Objets: {0}", itemInventory.Count);

        for (int i = 0; i < questPartyMembers.Count; i++)
        {
            Debug.LogFormat("Index: {0} - {1}", i, questPartyMembers[i]);

            if(questPartyMembers[i] == "Merlin le Magicien")
            {
                Debug.Log("Bravo vous êtes Merlin!");
            }
        }

        foreach (string partyMember in questPartyMembers)
        {
            Debug.LogFormat("{0} - Ici!", partyMember);
        }

        foreach (KeyValuePair<string, int> kvp in itemInventory)
        {
            Debug.LogFormat("Objets: {0} - {1}g", kvp.Key, kvp.Value);
        }

        while(playerLives > 0)
        {
            Debug.Log("Toujours en vie!");
            playerLives--;
        }

        Debug.Log("Joueur Mort...");

        Character hero = new Character();

        Character hero2 = hero;
        hero2.name = "Iron Man le Meilleur";
        hero2.PrintStatsInfo();
        hero.PrintStatsInfo();

        Character heroine = new Character("BlackWidow");
        heroine.PrintStatsInfo();

        Weapon huntingBow = new Weapon("Arc de chasse", 105);
        Weapon warBow = huntingBow;

        huntingBow.PrintWeaponStats();
        warBow.PrintWeaponStats();

        Paladin knight = new Paladin("Roi Arthur", huntingBow);
        knight.PrintStatsInfo();

        camTransform = this.GetComponent<Transform>();
        Debug.Log(camTransform.localPosition);

        directionLight = GameObject.Find("Directional Light");
        lightTransform = directionLight.GetComponent<Transform>();
        Debug.Log(lightTransform.localPosition);
    }

    // Update is called once per frame
    void Update()
    { 
        
    }


    void ComputeAge()
    {
        Debug.Log(currentAge + addedAge);
    }

    public int GenerateCharacter(string name, int level)
    {
        Debug.LogFormat("Personnage: {0} - Niveau: {1}", name, level);
        return level + 5;
    }

    public void PocketChange()
    {
        if (currentGold > 50)
        {
            Debug.Log("Vous êtes Riches");
        }
        else if (currentGold < 15)
        {
            Debug.Log("Rien à voler.");
        }
        else
        {
            Debug.Log("Ni Riche Ni Pauvre.");
        }
    }

    public void OpenTreasureChamber()
    {
        if(pureOfHeart && rareItem == "Relique")
        {
            if(!hasSecretIncantation)
            {
                Debug.Log("Vous avez l'esprit mais pas la connaissance.");
            }
            else
            {
                Debug.Log("Le trésor est vôtre mon héro");
            }
        }
        else
        {
            Debug.Log("Vous pouvez revenir plus tard.");
        }
    }

    public void SwitchingAround()
    {
        switch (characterAction)
        {
            case "Guerison":
                Debug.Log("Potion.");
                break;
            case "Attaque":
                Debug.Log("Aux Armes!");
                break;
            default:
                Debug.Log("Levez les boucliers.");
                break;
        }

        switch (diceRoll)
        {
            case 7:
            case 15:
                Debug.Log("Dommage Médiocre.");
                break;
            case 20:
                Debug.Log("Coup Critique!");
                break;
            default:
                Debug.Log("Coup manqué.");
                break;
        }
    }
}






