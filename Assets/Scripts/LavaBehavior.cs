﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaBehavior : MonoBehaviour
{
    public Vector3 checkPoint;
    GameBehavior gameManager;

    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameBehavior>();
    }

    private void OnTriggerEnter(Collider other)
    {
        other.transform.position = checkPoint;
        gameManager.HP -= 1;
    }
}
