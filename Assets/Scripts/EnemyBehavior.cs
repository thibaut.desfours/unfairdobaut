﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehavior : MonoBehaviour
{
    public Transform patrolRoute;
    public List<Transform> locations;
    public Transform player;
    public GameObject bullet;
    public float bulletSpeed = 35f;
    private float shootingPace = 1.50f;
    public float timeSinceShoot = 0.00f;

    public int EnemyLives
    {
        get { return _lives; }
        private set
        {
            _lives = value;
            if (_lives <= 0)
            {
                Destroy(this.gameObject);
                Debug.Log("Ennemi KO");
            }
        }
    }

    private int locationIndex = 0;
    private NavMeshAgent agent;
    private int _lives = 3;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.Find("Player").transform;
        //InitializePatrolRoute();
        //MoveToNextPatrolLocation();




    }

    void Update()
    {
        /*
        if (agent.remainingDistance < 0.2f && !agent.pathPending)
        {
            MoveToNextPatrolLocation();
        }
        */
        timeSinceShoot -= Time.deltaTime;
        if (timeSinceShoot <= 0.00f)
        {
            GameObject newBullet = Instantiate(bullet, this.transform.position + new Vector3(-2,3,0), this.transform.rotation) as GameObject;
            Rigidbody bulletRB = newBullet.GetComponent<Rigidbody>();
            bulletRB.velocity = new Vector3(-1, 0, 0) * bulletSpeed;
            timeSinceShoot = shootingPace;
        }
    }

    void InitializePatrolRoute()
    {
        foreach (Transform child in patrolRoute)
        {
            locations.Add(child);
        }
    }

    void MoveToNextPatrolLocation()
    {
        if (locations.Count == 0)
            return;

        agent.destination = locations[locationIndex].position;
        locationIndex = (locationIndex + 1) % locations.Count;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            agent.destination = player.position;
            Debug.Log("Ennemi détecté - Attaque");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "Player")
        {
            Debug.Log("Player hors de zone d'attaque - On reste en patrouille");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        /*if (collision.gameObject.name == "Bullet(Clone)")
        {
            EnemyLives -= 1;
            Debug.Log("Coup critique");
        }*/
    }
}
