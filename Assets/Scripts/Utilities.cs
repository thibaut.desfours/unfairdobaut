﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class Utilities
{
    public static int playerDeaths = 0;

    public static void RestartLevel()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1.0f;
        Debug.Log("Morts du joueur : " + playerDeaths);
        string message = UpdateDeathCount(ref playerDeaths);
        Debug.Log("Morts du joueur : " + playerDeaths);
    }

    public static bool RestartLevel(int sceneIndex)
    {
        if (sceneIndex < 0)
        {
            throw new System.ArgumentException("La scene ne peut pas être négative !");
        }

        SceneManager.LoadScene(sceneIndex);
        Time.timeScale = 1.0f;

        return true;
    }

    public static string UpdateDeathCount(ref int countReference)
    {
        countReference += 1;
        return "La prochaine fois vous serez au numéro " + countReference;
    }
}
