﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryList<T> where T : class
{
    private T _item;

    public T item
    {
        get
        {
            return _item;
        }
        set
        {
            _item = value;
        }
    }

    public InventoryList()
    {
        Debug.Log("Liste générique initialisée");
    }

    public void SetItem(T newItem)
    {
        _item = newItem;
        Debug.Log("Nouvel objet ajouté !");
    }
}
