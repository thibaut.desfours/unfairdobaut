﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformBehavior : MonoBehaviour
{
    public GameObject Player;
    public bool isCheckPoint = false;
    LavaBehavior lavaBehavior;

    private void Start()
    {
        lavaBehavior = GameObject.Find("Lava").GetComponent<LavaBehavior>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.gameObject.tag == "player")
        {
            Player.transform.parent = transform;
        }

        if (isCheckPoint)
        {
            Debug.Log("Position : " + gameObject.transform.position);
            lavaBehavior.checkPoint = gameObject.transform.position;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == Player)
        {
            Player.transform.parent = null;
        }
    }
}
